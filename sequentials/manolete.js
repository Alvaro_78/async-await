const {taskOne, taskTwo} = require('./tasks')

async function main() {
  try{
    console.time('Measuring time')
    const value_one = await taskOne()
    const value_two = await taskTwo()
    console.timeEnd('Measuring time')
  
    console.log('Task One Returned', value_one)
    console.log('Task One Returned', value_two)  
  }catch(error) {
    console.log(error)
  }
} 

main()