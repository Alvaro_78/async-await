const util = require('util')
const sleep = util.promisify(setTimeout)

async function taskOne() {
  try{
    throw new Error('SOME PROBLEM')
    await sleep(4000)
    return 'ONE VALUE'
  }catch(error) {
    console.log(error)
  }
}

async function taskTwo() {
  try{
    await sleep(2000)
    return 'TWO VALUE'  
  }catch(error) {
    console.log(error)
  }
}

module.exports = {
  taskOne,
  taskTwo
}