// Las funciones asincronas nos permiten hacer el código mucho más legible 

async const requestHandle = (req, res) => {

  try {
    const user = await User.findById(req.userId)
    const tasks = await Tasks.findById(user.tasksId)
    tasks.completed = true
    await tasks.save()
    res.send('Task Completed')
  }
  catch(error) {
    res.send(error)
  }

} 