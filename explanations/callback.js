// Esta función se le llama callback por que Node.js no espera, ejecuta todas las funciones, 
//para ejecutar un código que dependa de oro lo escribiremos dentro de la finción callback.
// Se llaman funciones callback por que después de ejecutarse hacemos que ejecuten otra función
const  requestHandle = (req, res) => {
  User.findById(req.userId, (err, user) => {
    if(err) {
      res.send(err)
    //Hacemos una busqueda por id a user, si hay un error nos devuelve res.send('con el error)
    }else {
      Tasks.findById(user.taskIsd, (err, tasks) => {
        if(err) {
          return res.send(err) 
        }else {
          tasks.completed = true
          tasks.save( (err) => {
            if(err) {
              return res.send(err)
            }else {
              res.send('Task Completed')
            }
          })
        }
      })
    }
  }) 
}