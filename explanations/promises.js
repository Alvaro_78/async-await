// Ejemplo de promesas (then-catch) hay que hacer un return con lo que esperamos que nos devuelva la promesa
// Son como los callbacks, pero hace más legible el código

const requestHandler = (req,res) => {
  User.findById(req.userId)
    .then((user) => {
      return Tasks.findById(user.tasksId)      
    })
    .then((tasks) => {
      tasks.completed = true
       return tasks.save()
    })
    .then(() => {
      res.send('Task Completed')
    })
    // La función catch va a recoger todos los errors, solo se invoca una vez
    .catch((errors) => {
      res.send(errors)
    })
}